﻿using System.Collections;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public static int coinsCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        Coin.coinsCount++;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
     * Metodo que se llama automaticamente cuando
     * otro collider entra en contacto con el
     * que tiene este script (en particular la moneda)
     */
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Coin.coinsCount--;
            if (Coin.coinsCount == 0) {
                Debug.Log("Has Ganado!");
                GameObject gameManager = GameObject.Find("GameManager");
                Destroy(gameManager);
                GameObject[] fireworksSystem = GameObject.FindGameObjectsWithTag("Fireworks");
                foreach (GameObject fireworks in fireworksSystem)
                {
                    fireworks.GetComponent<ParticleSystem>().Play();
                }
            }
            Destroy(gameObject);
        }
    }
}
