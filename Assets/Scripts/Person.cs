﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person
{
    private string firstName;
    private string lastName;
    private int age;
    public bool isMale;
    public Person spouse;

    public Person(string pFirstName, string pLastName)
    {
        this.firstName = pFirstName;
        this.lastName = pLastName;       
    }

    public Person(string firstName, string lastName, int age, bool isMale)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isMale = isMale;
    }

    public bool IsMarriedWith(Person otherPerson)
    {
        if (spouse == null)
        {
            return false;
        }
        else
        {
            if(otherPerson == this.spouse)
            {
                return true;
            }
            return false;
        }
    }
    public void setFirstName(string firstName)
    {
        this.firstName = firstName;
    }

    public string getFirstName()
    {
        return this.firstName;
    }
}
