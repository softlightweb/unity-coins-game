﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Family : MonoBehaviour
{
    public Person father;
    public Person mother;
    public Person son;

    // Start is called before the first frame update
    void Start()
    {
        father = new Person("Anakin", "Skywalker", 35, true);               

        mother = new Person("Padme", "Amidala", 28, false);                

        father.spouse = mother;
        mother.spouse = father;

        son = new Person("Luke", "Skywalker", 8, true);               

        if (father.IsMarriedWith(mother))
        {
            Debug.Log(father.getFirstName() + " y " + mother.getFirstName() + " están casados");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
