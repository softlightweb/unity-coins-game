﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionsScript : MonoBehaviour
{
    public string student1 = "Christian";
    public string student2 = "Kate";
    public string student3 = "Mia";
    public string student4 = "Anastasia";
    public string[] students = new string[] { "Christian", "Kate", "Mia", "Anastasia" };
    public string[] familyNames = new string[4];
    public List<string> studentsNames = new List<string>();
    public ArrayList inventory = new ArrayList();
    public Hashtable personalDetails = new Hashtable();
    // Start is called before the first frame update
    void Start()
    {
        studentsNames.Add(student1);
        studentsNames.Add(student2);
        studentsNames.Add(student3);
        studentsNames.Add(student4);

        if (studentsNames.Contains("Juan"))
        {
            studentsNames.Remove("Juan");
        }
        studentsNames.Insert(2, "Paul");
        string[] studentsNamesToArray = studentsNames.ToArray();
        studentsNames.Clear();

        inventory.Add(30);
        inventory.Add(3.141564);
        inventory.Add("Carlos");
        inventory.Add(false);
        inventory.Add(GameObject.FindGameObjectsWithTag("Fireworks"));
        Debug.Log(inventory[1].GetType());
        Debug.Log(inventory[4].GetType());

        personalDetails.Add("firstName","Rafa");
        personalDetails.Add("lastName", "Gonzalez Muñoz");
        personalDetails.Add("age", 26);
        personalDetails.Add("gender", "male");
        personalDetails.Add("isMarried", false);
        personalDetails.Add("hasChildren", false);
        if (personalDetails.Contains("firstName"))
        {
            Debug.Log(personalDetails["firstName"]);
            string name = (string)personalDetails["firstName"];
        }       
    }

    // Update is called once per frame
    void Update()
    {
         
    }
}
