﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopsScript : MonoBehaviour
{
    public string student1 = "Christian";
    public string student2 = "Kate";
    public string student3 = "Mia";
    public string student4 = "Anastasia";
    public List<string> studentsNames = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        studentsNames.Add(student1);
        studentsNames.Add(student2);
        studentsNames.Add(student3);
        studentsNames.Add(student4);

        foreach(string person in studentsNames)
        {
            Debug.Log(person);
        }
        for (int i = 0; i <= 10; i++)
        {
            Debug.Log(i);
        }

        int counter = 1;
        while (counter <= 10)
        {
            Debug.Log(counter);
            counter++;
        }
        for(int i = 0; i < 100; i++)
        {
            if (i == 0)
            {
                Debug.Log("El numero cero es especial...");
            } else if (IsNumberEven(i))
            {
                Debug.Log("El numero " + i + " es par");
            } else
            {
                Debug.Log("El numero " + i + " es impar");
            }
        }

        for (int number = 2; number <= 100; number++)
        {
            bool isPrime = true;
            for (int i = 2; i < number; i++)
            {
                if (number % i == 0)
                {
                    isPrime = false;
                }
            }
            if (isPrime)
            {
                Debug.Log("El numero " + number + " es primo");
            }          
        }

        int objectPos = -1;

        for (int i = 0; i < studentsNames.Count; i++)
        {
            if(studentsNames[i] == "Kate")
            {
                objectPos = i;
                break;
            }
        }
        if (objectPos == -1)
        {
            Debug.Log("No hemos encontrado el objeto que buscabas");
        }else
        {
            Debug.Log("El objeto buscado se encuentra en la posición " + objectPos);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool IsNumberEven(int number)
    {
        return number % 2 == 0;
    }
}
